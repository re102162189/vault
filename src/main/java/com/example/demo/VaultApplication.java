package com.example.demo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VaultApplication implements CommandLineRunner {
  
  @Value("${key}")
  private String secret;
  
  public static void main(String[] args) {
    SpringApplication.run(VaultApplication.class, args);
  }
  
  @Override
  public void run(String... args) {
    System.out.println(secret);
  }

}

disable_mlock     = true

log_level         = "info"
log_format        = "standard"

default_lease_ttl = "168h"
max_lease_ttl     = "0h"

api_addr          = "https://0.0.0.0:8200"

listener "tcp" {
  address         = "0.0.0.0:8200"
  #tls_disable     = "true"
  tls_cert_file   = "/vault/certs/vault.cert.pem"
  tls_key_file    = "/vault/certs/vault.key.pem"
}

storage "file" {
  "path"          = "/vault/file"
}
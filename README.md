## service preparation

### openssl for crt.pem & key.pem, make sure vault.cnf is on the same path
```
$ openssl req -x509 \
	-days 365 \
	-newkey rsa:4096 \
	-nodes \
	-out vault.cert.pem \
	-keyout vault.key.pem \
	-config vault.cnf
```

### openssl for .p12, use password 'vaultpwd' (or any you like)
```
$ openssl pkcs12 -export \
	-out vault.p12 \
	-inkey vault.key.pem \
	-in vault.cert.pem
```

### keytool (from jdk) for .jks, use password 'vaultpwd' (or any you like)
```
$ keytool -importkeystore \
	-srckeystore vault.p12 \
	-srcstoretype pkcs12 \
  	-ext SAN=dns:www.vault.com \
	-destkeystore vault.jks	
```

### add host name (www.vault.com) in /etc/hosts

### create a foldder called "vault" then create a folder "config" & "certs" inside it.

### copy "vault.hcl" under "/path/vault/config"

### copy "vault.cert.pem" & "vault.key.pem" (created by above instructions) under "/path/vault/certs"

### build vault
```
$ docker run --name vault -d -p 8200:8200 --cap-add=IPC_LOCK \
	-v /path/vault/config:/vault/config \
	-v /path/vault/policies:/vault/policies \
	-v /path/vault/file:/vault/file \
	-v /path/vault/logs:/vault/logs \
    -v /path/vault/certs:/vault/certs \
	vault:1.7.0 vault server -config=/vault/config/vault.hcl
```

### establish kv, policy, approle in vault
```
$ docker exec -it vault sh
/ # export VAULT_CACERT=/vault/certs/vault.crt
/ # vault operator init
Unseal Key 1: EfKDSyP97BiVO6QgNdncmvZwpMXzV+AhqXEhKE6xOUDZ
Unseal Key 2: j8u1/VyWj/xpieph/B7Pj0GYqNZBJqp//pjdoiV4QY/m
Unseal Key 3: bjlw4zhI63bfQDGZn5oTbE8xZuYL3Gg6HwErcTFGRVub
Unseal Key 4: XtmN11rGlo0C789JrpqzDFzJQiSQYJ0zWaYGSCDv7k8E
Unseal Key 5: 1KBwXG1gJe0KKcbCbbgSv5Lsp14S2Q1DjT/nb6VN6KKu

Initial Root Token: s.o4WkTueijIuV0Thf0x6BSCOu

# unseal vault 3 times & login use root token
/ # vault operator unseal
/ # vault login s.o4WkTueijIuV0Thf0x6BSCOu

# create secret
/ # vault secrets enable -path=secret kv
/ # vault kv put secret/pulse key1=123456

/ # vault policy write pulse - << EOF
path "secret/pulse" {
    capabilities = ["read"] 
}
EOF

# create approle
/ # vault auth enable approle
/ # vault write auth/approle/role/pulse policies="pulse"
/ # vault read auth/approle/role/pulse/role-id
Key        Value
---        -----
role_id    36ff4752-b8ec-9a1f-ba0f-996a1923b3be

/ # vault write -force auth/approle/role/pulse/secret-id
vault write -force auth/approle/role/pulse/secret-id
Key                   Value
---                   -----
secret_id             48d025d4-8108-ae65-b7a3-3bf7f1ad26a9
secret_id_accessor    7267cd18-b3f9-fd8f-2f00-b58627e7db6d
secret_id_ttl         0s

/ # exit
```

### copy the role_id & secret_id to boostrap.yml (DO NOT use id string above, it will be different in your environment)

### copy .jks under /src/main/resources

### see the result in console
```
$ mvn spring-boot:run
```
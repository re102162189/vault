#!/bin/bash

while true; do
    read -s -p "1st Password: " password_1st
    echo
    read -s -p "1st Password Confirm: " password_1st_confirm
    echo
    [ "$password_1st" = "$password_1st_confirm" ] && break
    echo "1st password and confirm does not match, Please try again"
done

while true; do
    read -s -p "2nd Password: " password_2nd
    echo
    read -s -p "2nd Password Confirm: " password_2nd_confirm
    echo
    [ "$password_2nd" = "$password_2nd_confirm" ] && break
    echo "2nd password and confirm does not match, Please try again"
done


echo -n $password_1st$password_2nd | vault kv put $1 $2=-
